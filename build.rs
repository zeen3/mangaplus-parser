extern crate pb_rs;
use pb_rs::types::{Config, FileDescriptor};
use std::fs::OpenOptions;
use std::io::{BufRead, BufReader, Write};
use std::path::PathBuf;
macro_rules! manifest_dir {
    () => {
        ::std::env::var("CARGO_MANIFEST_DIR").expect("CARGO_MANIFEST_DIR is not set")
    };
}
macro_rules! out_dir {
    () => {
        ::std::env::var("OUT_DIR").expect("OUT_DIR is not set")
    };
}
const READER: &str = "/reader.rs";
fn main() {
    println!("{:?}", manifest_dir!());
    let in_file: PathBuf = (manifest_dir!() + "/src/reader.proto").into();
    let out_file: PathBuf = (out_dir!() + READER).into();
    println!("in_file: {:?}, out_file: {:?}", in_file, out_file);
    let cfg = Config {
        in_file,
        out_file,
        single_module: false,
        import_search_path: vec![],
        no_output: false,
        error_cycle: true,
        headers: false,
        dont_use_cow: cfg!(feature = "no_cow"),
        custom_includes: if cfg!(feature = "serde") {
            vec!["use serde::{Serialize, Deserialize};".to_owned()]
        } else {
            vec![]
        },
        custom_struct_derive: if cfg!(feature = "serde") {
            vec!["Serialize, Deserialize".to_owned()]
        } else {
            vec![]
        },
        custom_rpc_generator: Box::new(|_, _| Ok(())),
        owned: false,
        nostd: false,
        hashbrown: true,
        custom_repr: None,
    };

    FileDescriptor::write_proto(&cfg).expect("written protobuf");
    if cfg!(feature = "serde") {
        let out_file = PathBuf::from(out_dir!() + READER);
        println!("{:?}", out_file);
        let file = OpenOptions::new()
            .read(true)
            .open(&out_file)
            .expect("a file out");
        let lines = BufReader::new(file)
            .lines()
            .map(|s| s.expect("line"))
            .collect::<Vec<String>>();
        let mut file_out = OpenOptions::new()
            .write(true)
            .truncate(true)
            .open(&out_file)
            .expect("a writable file");
        for line in lines.into_iter() {
            const SUPER: &'static str = "\nuse super::*;";
            const DERIVE: &'static str = "\n#[derive(Serialize, Deserialize)]";
            let new_line = match line.as_str() {
                "#[derive(Debug, PartialEq, Clone)]" => line + DERIVE,
                "#[derive(Debug, PartialEq, Eq, Clone, Copy)]" => SUPER.to_owned() + &line + DERIVE,
                s => s.into(),
            };
            writeln!(file_out, "{}", new_line,).expect("to write a line");
        }
    }
}
