#[cfg(feature = "serde")] extern crate serde;
extern crate quick_protobuf;
use quick_protobuf::BytesReader;
use crate::reader;
#[cfg(not(feature = "no_cow"))]
pub fn parse_mangaplus_proto(buf: &[u8]) -> Result<reader::Response<'_>, quick_protobuf::Error> {
    let len = buf.len();
    let mut bytes = BytesReader::from_bytes(buf);
    bytes.read_message_by_len(buf, len)
}
#[cfg(feature = "no_cow")]
pub fn parse_mangaplus_proto(buf: &[u8]) -> Result<reader::Response, quick_protobuf::Error> {
    let len = buf.len();
    let mut bytes = BytesReader::from_bytes(buf);
    bytes.read_message_by_len(buf, len)
}
