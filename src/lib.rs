//! This crate provides a convenience parser for use with mangaplus data.
//!
//! The organisation of this crate is zero. You get 1 function which consumes
//! a byte reference and the rest of it is a pregenerated set of files, of
//! which one is consumed instantly.
pub mod reader;
pub mod mangaplus;
pub use mangaplus::*;
