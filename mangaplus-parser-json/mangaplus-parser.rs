use {
    mangaplus_parser::reader::Response,
    serde_json::to_writer,
    std::{
        error::Error,
        io::{stdin, stdout, Read, Write},
    },
};

fn main() -> Result<(), Box<dyn Error>> {
    let mut buf = Vec::with_capacity(0xffff);
    stdin().read_to_end(&mut buf)?;
    let mut reader = quick_protobuf::reader::Reader::from_bytes(buf);
    let out = stdout();
    let mut out = out.lock();
    let resp: Response = match reader.read(|r, b| r.read_message_by_len::<Response>(b, b.len())) {
        Ok(resp) => resp,
        Err(quick_protobuf::Error::UnexpectedEndOfBuffer) => return Ok(()),
        Err(e) => return Err(Box::new(e)),
    };
    to_writer(&mut out, &resp)?;
    drop(resp);
    out.flush()?;
    Ok(())
}
