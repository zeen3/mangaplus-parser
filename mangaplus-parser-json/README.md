# mangaplus-parser-json

Binary section of [mangaplus-parser][], see [mangaplus-parser on crates.io][] for more details.

[mangaplus-parser]: https://gitlab.com/zeen3/mangaplus-parser
[mangaplus-parser on crates.io]: https://crates.io/crates/mangaplus-parser
