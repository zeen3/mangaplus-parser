# mangaplus-parser

- in binary (`cargo install mangaplus-parser-json`, `mangaplus-parser < mangaplus_data.bin`): parses the output (stdin) of mangaplus data and re-outputs as JSON for use with tools such as jq.
- in lib: offers a singular function to decode a slice, and a module for use with objects.

Build requirements:

- cargo

Recommended setup:

- install the musl stuff (`git clone git://git.musl-libc.org/musl`, follow INSTALL)
- install the musl toolchain (`rustup target add x86_64-unknown-linux-musl`)
- run `cargo build --release --target x86_64-unknown-linux-musl`
- run `cp target/x86_64-unknown-linux-musl/release/mangaplus-parser ~/.local/bin` if you need that

Recommended installs:

- [`jq`](https://stedolan.github.io/jq/)

Prebuilt binaries:

yeah I need to figure that out

Usage:

```shell
curl --http2-prior-knowledge -H SESSION-TOKEN:$(uuidgen) 'https://jumpg-webapi.tokyo-cdn.com/api/{}' | mangaplus-parser | jq .
```
